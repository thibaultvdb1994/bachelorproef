var videoData = [
	{//0
		'sourceUrl': 'assets/begin.webm', // WEBM
		'sourceUrlFallback': 'assets/01.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-hand-pointer-o"></i> Man',
				'goToVideo': 1,
			},
			{
				'name': '<i class="fa fa-hand-pointer-o"></i> Vrouw',
				'goToVideo': 16,
			}
		]
	},
	{//1
		'sourceUrl': 'assets/Jan 00.webm', // WEBM
		'sourceUrlFallback': 'assets/02.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-credit-card"></i> Pay with creditcard',
				'goToVideo': 2,
			},
			{
				'name': '<i class="fa fa-paypal"></i> Play with PayPal',
				'goToVideo': 9,
			}
		]
	},
	{//2
		'sourceUrl': 'assets/Jan 01.webm', // WEBM
		'sourceUrlFallback': 'assets/03.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-paperclip"></i> Give her a paperclip',
				'goToVideo': 3,
			},
			{
				'name': '<i class="fa fa-eraser"></i> Give her an eraser',
				'goToVideo': 6,
			}
		]
	},
	{//3
		'sourceUrl': 'assets/Jan 01-01.webm', // WEBM
		'sourceUrlFallback': 'assets/04.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-chevron-circle-left"></i> Look left',
				'goToVideo': 4,
			},
			{
				'name': '<i class="fa fa-chevron-circle-right"></i> Look right',
				'goToVideo': 5,
			}
		]
	},
	{//4
		'sourceUrl': 'assets/Jan 01-01-01.webm', // WEBM
		'sourceUrlFallback': 'assets/05.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-facebook"></i> Add as a friend',
				'goToVideo': 0,
			}
		]
	},
	{//5
		'sourceUrl': 'assets/Jan 01-01-02.webm', // WEBM
		'sourceUrlFallback': 'assets/06.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-unlock"></i> Unhook bra',
				'goToVideo': 0,
			}
		]
	},
	{//6
		'sourceUrl': 'assets/Jan 01-02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 7,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 8,
			}
		]
	},
	{//7
		'sourceUrl': 'assets/Jan 01-02-01.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//8
		'sourceUrl': 'assets/Jan 01-02-02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//9
		'sourceUrl': 'assets/Jan 02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 10,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 13,
			}
		]
	},
	{//10
		'sourceUrl': 'assets/Jan 02-01.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 11,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 12,
			}
		]
	},
	{//11
		'sourceUrl': 'assets/Jan 02-01-01.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//12
		'sourceUrl': 'assets/Jan 02-01-02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//13
		'sourceUrl': 'assets/Jan 02-02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 14,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 15,
			}
		]
	},
	{//14
		'sourceUrl': 'assets/Jan 02-02-01.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//15
		'sourceUrl': 'assets/Jan 02-02-02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//16
		'sourceUrl': 'assets/els 00.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 17,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 24,
			}
		]
	},
	{//17
		'sourceUrl': 'assets/els 01.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 18,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 21,
			}
		]
	},
	{//18
		'sourceUrl': 'assets/els 01-01.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 19,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 20,
			}
		]
	},
	{//19
		'sourceUrl': 'assets/els 03-neen.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//20
		'sourceUrl': 'assets/els 03-ja.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//21
		'sourceUrl': 'assets/els 01-02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 22,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 23,
			}
		]
	},
	{//22
		'sourceUrl': 'assets/els 03-neen.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//23
		'sourceUrl': 'assets/els 03-ja.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//24
		'sourceUrl': 'assets/els 02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 25,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 28,
			}
		]
	},
	{//25
		'sourceUrl': 'assets/els 02-01.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 26,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 27,
			}
		]
	},
	{//26
		'sourceUrl': 'assets/els 03-neen.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//27
		'sourceUrl': 'assets/els 03-ja.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//28
		'sourceUrl': 'assets/els 02-02.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 29,
			},
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 30,
			}
		]
	},
	{//29
		'sourceUrl': 'assets/els 03-neen.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
	{//30
		'sourceUrl': 'assets/els 03-ja.webm', // WEBM
		'sourceUrlFallback': 'assets/07.mp4', // FALLBACK
		'question': 'Choose your character.',
		'options': [
			{
				'name': '<i class="fa fa-eye"></i> Watch again',
				'goToVideo': 0,
			}
		]
	},
];
