/*
* Author: Thibault Van den Broecke
* Description: Script for managing the flow for 2-Story
* Created: 18-01-2016
* Modified: 20-01-2016
*/

(function() {

	var App = {

		init: function() {
			// Closure
			var self = this

			// Constants
			this.FADEOUTDURATION = 1000;
			this.WEBMISSUPPORTED = false;

			// Variables
			this.video = document.querySelector('#video');
			this.queuedVideos = [];

			// Check if Webm is supported
			this.checkWebmSupport();

			// Preload video
			//this.preloadVideo('http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4');

			this.currentVideo = 0;
			this.endMarker = 0;

			// SFX
			this.startVideoSFX = new Audio('audio/startvideo.wav');
			this.exitVideoSFX = new Audio('audio/exitvideo.wav');
			this.pauseVideoSFX = new Audio('audio/pausevideo.wav');
			this.SFX = [self.startVideoSFX, self.exitVideoSFX, self.pauseVideoSFX];

			// Start the first video - video 0
			this.newVideo(0);

			// Register event listeners
			self.registerEventListeners();
		},

		checkWebmSupport: function() {
		    // Closure
		    var self = this;

			if (Modernizr.video.webm) {
		        self.WEBMISSUPPORTED = true;
		    }

		},

		registerEventListeners: function() {
			// Closure
			var self = this;

			// Hotkeys
			window.addEventListener('keydown', function(ev) {
				if (ev.keyCode == 80) {
					self.togglePause();
				} else if (ev.keyCode == 77) {
					self.toggleMute();
				} else if (ev.keyCode == 70) {
					self.toggleFullscreen();
				}
			});

			// Add an event listener to that time
			self.video.addEventListener("timeupdate", function () {
			if (this.currentTime >= self.endMarker) {
				self.showOptions();
			}
			});

			// Register the fullscreen button
			var fullscreenButton = document.querySelector("button[data-role='fullscreen']");
			fullscreenButton.addEventListener('click', function() {
				self.toggleFullscreen();
			});

			// Register pause button
			var pauseButton = document.querySelector("button[data-role='pause']");
			pauseButton.addEventListener('click', function() {
				self.togglePause();
			});

			// Register mute button
			var muteButton = document.querySelector("button[data-role='mute']");
			muteButton.addEventListener('click', function() {
				self.toggleMute();
			});
		},

		registerOptionButtons: function() {
			/*
			* Registering the event listeners for the option buttons is outside the registerEventListeners-function
			* Because we'll have to register these buttons every time they get replaced
			* Callback of registerEventListeners every time the buttons get replaced is unnessecary
			*/

			// Closure
			var self = this

			// Register the option buttons who appear at the end of the video
			var optionButtons = document.querySelectorAll('button.btn');
			for (var i = 0; i < optionButtons.length; i++) {
				var button = optionButtons[i];
				button.addEventListener('click', function() {
					self.optionPressed(this.getAttribute('data-option'));
					console.log("Button pressed!");
				});
			}
		},

		calcEndMarker: function() {
			// Closure
			var self = this;

			// Calculate the time where the video should start the fadeOut
			self.endMarker = self.video.duration - (self.FADEOUTDURATION / 1000);

			console.log("Video duration:" + self.video.duration);
			console.log("End marker:" + self.endMarker);
		},

		showOptions: function() {
			// Closure
			var self = this;

			var overlay = document.querySelector('.overlay[data-role="buttons"]');
			overlay.classList.add('overlay-active');

			window.setTimeout(function() {
				self.video.pause();
			}, 500);

			// Play SFX
			self.exitVideoSFX.play();
			self.video.setAttribute('poster', videoData[self.currentVideo].posterUrl);
		},

		hideButtons: function() {
			var overlay = document.querySelector('.overlay[data-role="buttons"]');
			overlay.classList.remove('overlay-active');
		},

		optionPressed: function(option) {
			// Closure
			var self = this;

			self.hideButtons();

			//self.video.load();
			self.newVideo(option);
		},

		newVideo: function(video) {
			// Closure
			var self = this;

			// Give self.currentvideo the value of the option
			self.currentVideo = video;

			// Replace the current video with the chosen one
			self.video.src = videoData[self.currentVideo].sourceUrl;

			// if (self.WEBMISSUPPORTED) {
			// 	self.video.src = videoData[self.currentVideo].sourceUrl;
			// } else {
			// 	self.video.src = videoData[self.currentVideo].sourceUrlFallback;
			// }

			// Load the video
			self.video.load();

			// Play SFX
			self.startVideoSFX.play();

			// Execute when video is loaded
			self.video.addEventListener('canplaythrough', function(ev) {
				// Remove the event listener
				ev.target.removeEventListener(ev.type, arguments.callee);

				// Calculate when we should start the transition
				self.calcEndMarker();

				// Replace the option buttons
				self.replaceOptions();

				// Change the poster image
				self.replacePosterImage();
			});

			// Preload next two videos
		},

		replaceOptions: function() {
			// Closure
			var self = this;

			window.setTimeout(function() {
				var buttonContainer = document.querySelector('.options');
				var tempHTML = "";
				for (var i = 0; i< videoData[self.currentVideo].options.length; i++) {
					var option = videoData[self.currentVideo].options[i];
					tempHTML += "<button class='btn' data-option=" + option.goToVideo + ">"
					tempHTML += option.name
					tempHTML += "</button>"
				}
				buttonContainer.innerHTML = tempHTML;

				// Register the new options
				self.registerOptionButtons();
			},1000);
		},

		replacePosterImage: function() {
			// Closure
			var self = this

			//self.video.setAttribute('poster', videoData[self.currentVideo].posterUrl);
			//self.video.style.backgroundImage = "url(" + videoData[self.currentVideo].posterUrl + ")";
		},

		toggleFullscreen: function() {
			// Closure
			var self = this;

			var fullScreenButton = document.querySelector('.control[data-role="fullscreen"]');

			if ((document.fullScreenElement && document.fullScreenElement !== null) ||
			(!document.mozFullScreen && !document.webkitIsFullScreen)) {
				// Go full screen
				if (document.documentElement.requestFullScreen) {
					document.documentElement.requestFullScreen();
				} else if (document.documentElement.mozRequestFullScreen) {
					document.documentElement.mozRequestFullScreen();
				} else if (document.documentElement.webkitRequestFullScreen) {
					document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
				}

				fullScreenButton.innerHTML = '<span class="icon-resize-100"></span>';
			} else {
				// Exit full screen
				if (document.cancelFullScreen) {
					document.cancelFullScreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.webkitCancelFullScreen) {
					document.webkitCancelFullScreen();
				}

				fullScreenButton.innerHTML = '<span class="icon-resize-full-screen"></span>';
			}
		},

		togglePause: function() {
			// Closure
			var self = this;

			var overlay = document.querySelector('.overlay[data-role="pause"]');
			var pauseButton = document.querySelector('.control[data-role="pause"]');

			// If video is paused, play it
			if (self.video.paused) {
    			self.video.play();
    			overlay.classList.remove('overlay-active');

    			// Change the icon
    			pauseButton.innerHTML = '<span class="icon-controller-paus"></span>';
    		} else { // If video is unpaused, pause it
    			overlay.classList.add('overlay-active');
    			window.setTimeout(function() {
    			self.video.pause();
    			// Change the icon
    			pauseButton.innerHTML = '<span class="icon-controller-play"></span>';
    			}, 500);
    		}

    		// Play SFX
    		self.pauseVideoSFX.play();
    	},

		toggleMute: function() {
    		// Closure
    		var self = this;

			var muteButton = document.querySelector('.control[data-role="mute"]');

			self.video.muted = !self.video.muted;

			if (self.video.muted) {
				muteButton.innerHTML = '<span class="icon-sound-mute"></span>';
			} else {
				muteButton.innerHTML = '<span class="icon-sound"></span>';
			}

    		// (Un)mute SFX
    		for (var i = 0; i < self.SFX.length; i++) {
    		    self.SFX[i].muted = !self.SFX[i].muted;
    		}
		},

		preloadVideo: function(source, index) {
		    // Closure
		    var self = this;

			var video = document.createElement("VIDEO")
			video.src = source;
			//video.load();

			video.addEventListener('loadeddata', function() {
				console.log("VIDEO " + index + " IS GELADEN!!!");
			});
		},
	}

	App.init();
})();
